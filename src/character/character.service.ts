import { Injectable, Logger } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { RedisManagerService } from 'src/_db/redis/redis.service';

const logger = new Logger("CharacterService");

@Injectable()
export class CharacterService {
    constructor(
        private RedisManagerService: RedisManagerService
    ) { }

    public async addCharacter(name: string, highLevel: number, timeEnd: number) {
        try {
            const playerData = {
                name,
                highest_level: highLevel,
                time: timeEnd, //Sec
            }

            const key = Object.keys(playerData);
            const value = Object.values(playerData);

            const fieldData = [];
            for (let i = 0; i < key.length; i++) {
                const targetFieldData = [key[i], value[i]];
                fieldData.push(targetFieldData);
            }

            await this.RedisManagerService.hashSetData(`playerData:${randomUUID()}`, fieldData);
            logger.log("Add Character Success");
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }
}
