
import { Module } from '@nestjs/common';
import { RedisDataManagerModule } from 'src/_db/redis/redis.module';
import { CharacterService } from './character.service';

@Module({
  imports: [RedisDataManagerModule],
  providers: [CharacterService],
  exports: [CharacterService],
})
export class CharacterModule {}
