import { load } from 'ts-dotenv';

const config = load({
    ENV_TYPE: String,
    PORT: Number,

    REDIS_CONNECT_URL: String,
    REDIS_CONNECT_PORT: Number,
    REDIS_CONNECT_INDEX: Number,

    IS_ENABLE_DOCS: Boolean
});

export const ENV_TYPE = config.ENV_TYPE;
export const PORT = config.PORT;

export const REDIS_CONNECT_URL = config.REDIS_CONNECT_URL;
export const REDIS_CONNECT_PORT = config.REDIS_CONNECT_PORT;
export const REDIS_CONNECT_INDEX = config.REDIS_CONNECT_INDEX;

export const IS_ENABLE_DOCS = config.IS_ENABLE_DOCS;
