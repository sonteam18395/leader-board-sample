import { RedisClientType, RedisDefaultModules } from "redis";

export type RedisService = RedisClientType<RedisDefaultModules>;


export enum ERedisProvider {
    MAIN = 'REDIS_MAIN',
}