import { Logger } from '@nestjs/common';
import { createClient } from 'redis';
import {
    REDIS_CONNECT_INDEX,
    REDIS_CONNECT_PORT,
    REDIS_CONNECT_URL
} from 'src/config';
import { ERedisProvider, RedisService } from './redis.type';

const logger = new Logger('RedisService'); 

export const RedisProvider = {
    provide: ERedisProvider.MAIN,
    useFactory: async (): Promise<RedisService> => {
        try {
            const client: RedisService = await createClient({
                url: `redis://${REDIS_CONNECT_URL}:${REDIS_CONNECT_PORT}`,
                database: REDIS_CONNECT_INDEX
            });

            await client.connect();
            logger.log(`redis connect success: redis://${REDIS_CONNECT_URL}:${REDIS_CONNECT_PORT}`);

            return client;
        } catch (error) {
            throw error;
        }
    }
};
