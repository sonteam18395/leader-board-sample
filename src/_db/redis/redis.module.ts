import { Module } from '@nestjs/common';
import { Global } from '@nestjs/common/decorators';
import { RedisProvider } from './redis.provider';
import { RedisManagerService } from './redis.service';

@Global()
@Module({
    providers: [RedisProvider, RedisManagerService],
    exports: [RedisManagerService]
})
export class RedisDataManagerModule { }
