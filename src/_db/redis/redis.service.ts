import { Inject, Injectable, Logger } from '@nestjs/common';
import { ERedisProvider, RedisService } from './redis.type';
import { ZMember } from '@redis/client/dist/lib/commands/generic-transformers';


const logger = new Logger('RedisService');

@Injectable()
export class RedisManagerService {
    constructor(
        @Inject(ERedisProvider.MAIN)
        private redisService: RedisService,
    ) { }

    public async getData<T>(key: string): Promise<T | string | null> {
        try {
            const response = await this.redisService.get(key);
            if (!response) return null;

            try {
                return JSON.parse(response);
            } catch (error) {
                return response;
            }
        } catch (err) {
            logger.error(err.message, err.stack);
        }
    }

    public async setData(key: string, value: string, ttl: number = null): Promise<void> {
        try {
            if (ttl) {
                await this.redisService.set(key, value);
                await this.redisService.expire(key, ttl);
            } else {
                await this.redisService.set(key, value);
            }
        } catch (err) {
            logger.error(err.message, err.stack);
        }
    }

    //#region List
    public async listGetLength(key: string) {
        try {
            const result = await this.redisService.LLEN(key);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async listGet(key: string, start: number, end = -1) {
        try {
            const result = await this.redisService.LRANGE(key, start, end);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async listLPush(key: string, element: string | string[]) {
        try {
            const result = await this.redisService.LPUSH(key, element);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async listLPop(key: string) {
        try {
            const result = await this.redisService.LPOP(key);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }
    //#endregion

    //#region Hash
    public async hashSetData(key: string, value: Array<[string, string]>) {
        try {
            await this.redisService.HSET(key, value)
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async hashGetData(key: string, field: string | string[]): Promise<string | string[]> {
        try {
            if (typeof field == 'string') {
                const result = await this.redisService.HGET(key, field);
                return result;
            }
            const result = await this.redisService.HMGET(key, field);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async hashDeleteData(key: string, field: string | string[]) {
        try {
            const result = await this.redisService.HDEL(key, field);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }
    //#endregion

    //#region SortSet
    public async ZAddData(key: string, value: ZMember | ZMember[], ttl?: number): Promise<number> {
        try {
            const result = await this.redisService.ZADD(key, value);
            if (typeof (ttl) != 'undefined') {
                await this.redisService.EXPIRE(key, ttl);
            }
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e
        }
    }

    public async ZIncByMember(key: string, increment: number, member: string): Promise<number> {
        try {
            const result = await this.redisService.ZINCRBY(key, increment, member);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }

    // public redisZReverseRangeWithScore(key: string, start: number, stop: number): Promise<string[]> {

    //         try {
    //             const result = await this.redisService.(key, start, stop, 'WITHSCORES');
    //         } catch(e){

    //         }       
    // }

    public async ZRank(key: string, member: string): Promise<number> {
        try {
            const result = await this.redisService.ZRANK(key, member);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }

    public async ZReverseRank(key: string, member: string): Promise<number> {
        try {
            const result = await this.redisService.ZREVRANK(key, member);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }

    public async ZScore(key: string, member: string): Promise<number> {
        try {
            const result = await this.redisService.ZSCORE(key, member);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }

    //#endregion

    //#endregion
    public async IncreaseBy(key: string, increment: number) {
        try {
            const result = await this.redisService.INCRBY(key, increment);
            return result;
        } catch (e) {
            logger.error(e.message, e.stack);
            throw e;
        }
    }
}
