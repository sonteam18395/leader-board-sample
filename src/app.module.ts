import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisDataManagerModule } from './_db/redis/redis.module';
import { LeaderBoardModule } from './leaderboard/leaderboard.module';

@Module({
  imports: [RedisDataManagerModule, LeaderBoardModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
