
import { Module } from '@nestjs/common';
import { RedisDataManagerModule } from 'src/_db/redis/redis.module';
import { LeaderBoardService } from './leaderboard.service';
import { CharacterModule } from 'src/character/character.module';
import { LeaderBoardController } from './leaderboard.controller';

@Module({
    imports: [CharacterModule, RedisDataManagerModule],
    controllers: [LeaderBoardController],
    providers: [LeaderBoardService],
})
export class LeaderBoardModule { }
