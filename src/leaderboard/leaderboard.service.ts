import { Injectable, Logger } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { RedisManagerService } from 'src/_db/redis/redis.service';
import { CharacterService } from 'src/character/character.service';
import { ZMember } from '@redis/client/dist/lib/commands/generic-transformers';

const logger = new Logger("LeaderBoardService");
@Injectable()
export class LeaderBoardService {
    constructor(
        private CharacterService: CharacterService,
        private RedisManagerService: RedisManagerService
    ) { }

    public async addDataCharacter() {
        const inputName = ["Hen", "Hell", "Know"]
        for (let i = 0; i < inputName.length; i++) {
            const randomHighLevel = Math.round(Math.random() * 10);
            const randomTime = Math.round(Math.random() * 100);

            await this.CharacterService.addCharacter(inputName[i], randomHighLevel, randomTime);
            this.addDataLeaderboard(inputName[i], randomHighLevel, randomTime);
        }
    }

    public async addDataLeaderboard(name: string, highLevel: number, timeEnd: number) {
        const resultScore = highLevel + (0.5 / timeEnd);
        await this.RedisManagerService.ZAddData(`leaderboard`, { value: name, score: resultScore });
    }
}
