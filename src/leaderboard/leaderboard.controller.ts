import { Controller, Get } from '@nestjs/common';
import { LeaderBoardService } from './leaderboard.service';

@Controller('leaderboard')
export class LeaderBoardController {
    constructor(private readonly LeaderboardService: LeaderBoardService) { }

    @Get('add-data')
    async addData(): Promise<{ message: string }> {
        await this.LeaderboardService.addDataCharacter();
        return { message: 'add success' };
    }
}
